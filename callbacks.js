function Clock () {
  var currentTime = new Date();
  var seconds = currentTime.getSeconds();
  var minutes = currentTime.getMinutes();
  var hours = currentTime.getHours();

  setInterval(function () {
    seconds += 5;

    if (seconds >= 60) {
      minutes += 1;
      seconds -= 60;
    }

    if (minutes >= 60) {
      hours += 1;
      minutes -= 60;
    }

    console.log(hours + ":" + minutes + ":" + seconds);
  }, 5000);
}

// new Clock();

var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function addNumbers(sum, numsLeft, completionCallback) {
  this.sum = sum;
  this.numsLeft = numsLeft;

  if (numsLeft > 0) {
    reader.question("Enter a number: \n", function(answer) {
      sum += parseInt(answer);
      numsLeft -= 1;
      console.log("The current sum is: " + sum);
      addNumbers(sum, numsLeft, completionCallback);
    });
  } else {
    completionCallback(sum);
    reader.close();
  }
}

// addNumbers(0,3, function(sum) {
//   console.log("Total Sum: " + sum);
// });

function askLessThan(el1, el2, callback) {

  reader.question("Is " + el1 + " smaller than " + el2 + " ?", function(answer) {
    if (answer === "yes") {
      callback(true);
    } else {
      callback(false);
    }
  });
}

function performSortPass(arr, i, madeAnySwaps, callback) {

  if (i < (arr.length-1)) {
    askLessThan(arr[i], arr[i+1], function(lessThan) {
      if (lessThan === false) {
        temp = arr[i+1];
        arr[i+1] = arr[i];
        arr[i] = temp;
        madeAnySwaps = true;
      }
      performSortPass(arr, i+1, madeAnySwaps, callback);
    });
  } else if (i === (arr.length - 1)) {
    callback(madeAnySwaps);
  }
}

function crazyBubbleSort(arr, sortCompletionCallback) {

  function sortPassCallback(madeAnySwaps) {
    if (madeAnySwaps === true) {
      performSortPass(arr, 0, false, sortPassCallback);
    } else {
      sortCompletionCallback(arr);
      reader.close();
    }

  }

  sortPassCallback(true);
}

crazyBubbleSort([5,2,4,3,1], function (arr) { console.log(arr) });