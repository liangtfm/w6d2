

(function (root) {

  var readline = require('readline');

  var reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  var Hanoi = root.Hanoi = (root.Hanoi || {});

  var Game = Hanoi.Game = function () {
    this.board = [ [3,2,1], [], [] ];
  }

  Game.prototype.gameOver = function() {
    return ((this.board[0].length == 0) && (this.board[1].length == 0));
  }

  Game.prototype.getInput = function (isLegal, makeMove) {
    console.log(this.board);
    console.log("Make a move");
    that = this;

    reader.question("What is the starting tower? (1-3) \n", function(startString) {
      reader.question("What is the ending tower? (1-3) \n", function(endString) {
        var startTower = (parseInt(startString)-1);
        var endTower = (parseInt(endString)-1);

        if (that.isLegal(startTower, endTower)) {
          that.makeMove(startTower, endTower);
          if (that.gameOver() === true) {
            console.log("You win!");
            reader.close();
          } else {
            that.getInput(that.isLegal, that.makeMove);
          }
        } else {
          console.log("Invalid Move!");
          that.getInput(that.isLegal, that.makeMove);
        }

      });
    });
  }

  Game.prototype.isLegal = function(start, end) {
    var startTower = this.board[start];
    var endTower = this.board[end];

    if (startTower.length > 0) {
      if (endTower.length > 0) {
        if (startTower[startTower.length - 1] < endTower[endTower.length - 1]) {
          return true;
        }

        else {
          return false;
        }
      }

      else {
        return true;
      }
    }

    else {
      return false;
    }
  }

  Game.prototype.makeMove = function(start, end) {
    var startTower = this.board[start];
    var endTower = this.board[end];

    endTower.push(startTower.pop());
  }

  game = new Game();
  game.getInput(game.isLegal, game.makeMove);

})(this);

