(function (root) {

  var readline = require('readline');

  var reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  var TTT = root.TTT = (root.TTT || {});


  function Player(symbol) {
    this.symbol = symbol;
  }

  var Game = TTT.Game = function () {
    this.board = [[null, null, null], [null, null, null], [null, null, null]];
    this.player1 = new Player("X");
    this.player2 = new Player("O");
    this.currentPlayer = this.player1;
  }

  Game.prototype.render = function() {
    for(var i=0; i<3; i++) {
      console.log(this.board[i]);
    }
  }

  Game.prototype.makeMove = function(x,y,symbol) {
    this.board[x][y] = symbol;
  }

  Game.prototype.validMove = function(x,y) {
    return (this.board[x][y] === null);
  }

  Game.prototype.winner = function(symbol) {
    var board = this.board;

    var condition =
    (
    (this.on(0,0,symbol) && this.on(0,1,symbol) && this.on(0,2,symbol)) ||
    (this.on(1,0,symbol) && this.on(1,1,symbol) && this.on(1,2,symbol)) ||
    (this.on(2,0,symbol) && this.on(2,1,symbol) && this.on(2,2,symbol)) ||
    (this.on(0,0,symbol) && this.on(1,0,symbol) && this.on(2,0,symbol)) ||
    (this.on(0,1,symbol) && this.on(1,1,symbol) && this.on(2,1,symbol)) ||
    (this.on(0,2,symbol) && this.on(1,2,symbol) && this.on(2,2,symbol)) ||
    (this.on(0,0,symbol) && this.on(1,1,symbol) && this.on(2,2,symbol)) ||
    (this.on(0,2,symbol) && this.on(1,1,symbol) && this.on(2,0,symbol))
    );

    return condition;
  }

  Game.prototype.on = function(x,y,symbol) {
    return (this.board[x][y] === symbol);
  }

  Game.prototype.over = function() {
    return (this.winner("X") || this.winner("O"));
  }

  Game.prototype.play = function() {
    this.render();
    console.log("It's your turn, " + this.currentPlayer.symbol + "!");
    that = this;

    reader.question("Which row? (1-3) \n", function(row) {
      reader.question("Which column? (1-3) \n", function(column) {
        var x = (parseInt(row)-1);
        var y = (parseInt(column)-1);

        if (that.validMove(x, y) === true) {
          that.makeMove(x, y, that.currentPlayer.symbol);

          if (that.over()) {
            console.log(that.currentPlayer.symbol + " wins!");
            that.render();
            reader.close();
          } else {
            that.currentPlayer =
            (that.currentPlayer === that.player2) ? that.player1 : that.player2;
            that.play();
          }
        }
      });
    });

  }

  g = new Game();
  g.play();

})(this);